# Copyright (C) 2021  European Spallation Source ERIC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# The following lines are required
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


EXCLUDE_ARCHS += linux-corei7-poky
EXCLUDE_ARCHS += linux-ppc64e6500

REQUIRED += sequencer
APP := dtlcavm2App
APPDB := $(APP)/Db
APPSRC := $(APP)/src

SOURCES += $(APPSRC)/tuners_tank010.st
SOURCES += $(APPSRC)/tuners_tank020.st
SOURCES += $(APPSRC)/tuners_tank030.st
SOURCES += $(APPSRC)/tuners_tank040.st
SOURCES += $(APPSRC)/tuners_tank050.st
SOURCES += $(APPSRC)/arcDetection_tank010.st
SOURCES += $(APPSRC)/arcDetection_tank020.st
SOURCES += $(APPSRC)/arcDetection_tank030.st
SOURCES += $(APPSRC)/arcDetection_tank040.st
SOURCES += $(APPSRC)/arcDetection_tank050.st

TEMPLATES += $(wildcard $(APPDB)/*.db)

SCRIPTS += $(wildcard ../iocsh/*.iocsh)
SCRIPTS += $(wildcard ../iocsh/*.cmd)

.PHONY: vlibs
vlibs:
